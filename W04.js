//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use length and width. 

var App = {
  launch: function () {
    App.getName();
    App.getOrganizationName();
    App.getRows();
    App.getSeats();
    App.calculateSeats();
    App.getEstimate();
    App.displayExploreButtons();
    App.showExample();
    App.rememberClicks();
  },
  getName: function () {
    let answer = prompt("What is your first name", "Notorious");
    if (answer != null) {
      $("#first").html(answer); // $ = jQuery object, uses CSS selectors
      sessionStorage.firstName = answer
    }
  },
  getOrganizationName: function () {
    const s = sessionStorage.firstName + ", what is your Organization"
    let answer = prompt(s, "Nora");
    if (answer != null) {
      $("#last").html(answer);  // setter
    }
  },
  getRows: function () {
    let answer = prompt("How many rows do you want to reserve", 5);
    if (answer != null) {
      $('#width').html(answer);   // either double or single tick marks designate strings
    }
  },
  getSeats: function () {
    let answer = prompt("How many seats per row do you want to reserve", 5);
    if (answer != null) {
      $('#length').html(answer);  // html method works as a getter and a setter
    }
  },
  calculateSeats: function () {
    let inputWidth = parseFloat($('#width').html());
    let inputLength = parseFloat($('#length').html());
    let answer = inputWidth * inputLength;
    $("#area").html(answer);
    $(".displayText").css('display', 'inline-block');  //overwrites display: hidden to make it visible 
    alert(answer + " of students that will have a seat ");
  },
  calculateNumberOfSeats: function (givenWidth, givenLength) {

    // calculate the answer and store in a local variable so we can watch the value
    let area = width * length;
    // return the result of our calculation to the calling function
    return area;
  },
  getEstimate: function () {
    let area = parseFloat(document.getElementById("area").innerHTML);
    let ct;
    if (area < 1) { ct = 0; }
    else { ct = area }; // estimate 1 per square mile
    // document.getElementById("count").innerHTML = count;
    $("#count").html(ct);
    alert("You could have about " + ct + " sheep.");
    $("#count").css("color", "blue");
    $("#count").css("background-color", "yellow");
  },
  showExample: function () {
    document.getElementById("displayPlace").innerHTML = "";
    let totalCount = parseFloat($("#count").html());
    for (var i = 0; i < totalCount; i++) {
      App.addImage(i);
    }
  },
  addImage: function (icount) {
    var imageElement = document.createElement("img");
    imageElement.id = "image" + icount;
    imageElement.class = "picture";
    imageElement.style.maxWidth = "90px";
    var displayElement = document.getElementById("displayPlace");
    displayElement.appendChild(imageElement);
    document.getElementById("image" + icount).src = "bearcat.jpg";
    // document.getElementById("image" + icount).src = "59-images-of-baby-lamb-clipart-you-can-use-these-free-cliparts-for-sEfudv-clipart.jpg";
  },
  displayExploreButtons: function () {
    $(".displayExploreButtons").css('display', 'block');  //overwrites display: hidden to make it visible 
  },
  exploreHtml: function () {
    alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
      "Hit CTRL-F and search for displayPlace to see the new image elements you added to the page.\n")
  },
  exploreCode: function () {
    alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
      "In the window on the left, click on the .js file.\n\n" +
      "In the window in the center, click on the line number of the App.getName() call to set a breakpoint.\n\n" +
      "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
      "Up on the web page, click the main button to launch the app.\n\n" +
      "Execution of the code will stop on your breakpoint.\n\n" +
      "Hit F11 to step into the App.getName() function.\n" +
      "Hit F10 to step over the next function call.\n\n" +
      "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
      "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
    )
  },
  rememberClicks: function () {
    if (localStorage.getItem("clicks")) { // use getter
      const value = Number(localStorage.clicks) + 1  // or property
      localStorage.setItem("clicks", value)  // use setter
    } else {
      localStorage.clicks = 1 // or property
    }
    s = "You have clicked this button " + localStorage.clicks + " times"
    $("#clicks").html(s) // display forever clicks 
  }
};

